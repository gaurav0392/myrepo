select sum(cs_ext_discount_amt) as "excess discount amount" 
from 
 catalog_sales 
 ,item 
 ,date_dim
where
i_manufact_id = 269
and i_item_sk = cs_item_sk 
and d_date between cast('1998-03-18' as date) and
 (cast('1998-03-18' as date) + interval 90 days)
and d_date_sk = cs_sold_date_sk 
and cs_ext_discount_amt 
 > ( 
 select 
 1.3 * avg(cs_ext_discount_amt) 
 from 
 catalog_sales 
 ,date_dim
 where 
 cs_item_sk = i_item_sk 
 and d_date between cast('1998-03-18' as date) and
 (cast('1998-03-18' as date) + interval 90 days)
 and d_date_sk = cs_sold_date_sk 
 ) 
limit 100
select i_item_id
 ,i_item_desc
 ,i_current_price
 from item, inventory, date_dim, catalog_sales
 where i_current_price between 22 and 22 + 30
 and inv_item_sk = i_item_sk
 and d_date_sk=inv_date_sk
 and d_date between cast('2001-06-02' as date) and (cast('2001-06-02' as date) + interval 60 days)
 and i_manufact_id in (678,964,918,849)
 and inv_quantity_on_hand between 100 and 500
 and cs_item_sk = i_item_sk
 group by i_item_id,i_item_desc,i_current_price
 order by i_item_id
 limit 100

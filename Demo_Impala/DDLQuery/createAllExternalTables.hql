
create external table call_center(
      cc_call_center_sk         bigint               
,     cc_call_center_id         varchar(20)             
,     cc_rec_start_date         varchar(10)                         
,     cc_rec_end_date           varchar(45)                        
,     cc_closed_date_sk         bigint                       
,     cc_open_date_sk           bigint                       
,     cc_name                   varchar(30)                   
,     cc_class                  char                   
,     cc_employees              int                       
,     cc_sq_ft                  int                       
,     cc_hours                  char                      
,     cc_manager                char                   
,     cc_mkt_id                 int                       
,     cc_mkt_class             varchar(200)                      
,     cc_mkt_desc              varchar(50)                  
,     cc_market_manager        varchar(25)                   
,     cc_division               int                       
,     cc_division_name          char                   
,     cc_company                int                       
,     cc_company_name           char                      
,     cc_street_number          char                      
,     cc_street_name            char                   
,     cc_street_type            char                      
,     cc_suite_number           char                      
,     cc_city                   char                   
,     cc_county                 char                   
,     cc_state                  char                       
,     cc_zip                    char                      
,     cc_country                char                   
,     cc_gmt_offset             double                  
,     cc_tax_percentage         double
)
row format delimited fields terminated by '|' 
location '${hiveconf:LOCATION}/call_center';

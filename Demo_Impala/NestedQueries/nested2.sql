insert overwrite table scct_work_db.dy_pyrl_pos_audited_tmp
select
LGCY_STO_NBR
        ,BRND_CD                
        ,ASSC_SSN_NBR    
        ,CLK_PNCH_IN_DTTM
        ,CLK_PNCH_OUT_DTTM
        ,round(DAY(cast(CLK_PNCH_OUT_DTTM as timestamp) - cast(CLK_PNCH_IN_DTTM as timestamp))*24
+ HOUR(cast(CLK_PNCH_OUT_DTTM as timestamp) - cast(CLK_PNCH_IN_DTTM as timestamp))
+ MINUTE(cast(CLK_PNCH_OUT_DTTM as timestamp) - cast(CLK_PNCH_IN_DTTM as timestamp))/60
+ SECOND(cast(CLK_PNCH_OUT_DTTM as timestamp) - cast(CLK_PNCH_IN_DTTM as timestamp))/(60*60),2) as PYRL_HR_AMT
        ,POS_PYRL_CD
        ,' ' as HRLY_IND
        ,date_Sub(days_Add(AddDate(cc_rec_start_date,2),4), interval 3 weeks) as TRXN_POST_DT
        ,CLK_PNCH_EVNT_ID
        ,LST_MOD_DTTM
        ,LST_MOD_PTY_ID_VAL
        ,REC_INS_DTTM
        ,pyrl_tme_typ_nme
        ,prod_pyrl_ind
        ,tplh_flg_ind
FROM 
(select
LGCY_STO_NBR
        ,BRND_CD                
        ,ASSC_SSN_NBR    
        ,CLK_PNCH_IN_DTTM
        ,case when datediff(clk_pnch_out_dttm,clk_pnch_in_dttm) >2 then concat(date_add(clk_pnch_in_dttm,1) ,' ','00:00:00') 
        else clk_pnch_out_dttm end as CLK_PNCH_OUT_DTTM
        ,POS_PYRL_CD
        ,CLK_PNCH_EVNT_ID
        ,LST_MOD_DTTM
        ,LST_MOD_PTY_ID_VAL
        ,REC_INS_DTTM
        ,pyrl_tme_typ_nme
        ,prod_pyrl_ind
        ,tplh_flg_ind
FROM (
   SELECT  POS.LGCY_STO_NBR 
 ,POS.BRND_CD AS BRND_CD    
        ,POS.PTY_ID_VAL   
        ,POS.PTY_ID_VAL as ASSC_SSN_NBR 
        ,POS.CLK_PNCH_IN_DTTM  
        ,cast(POS.CLK_PNCH_IN_DTTM as date) AS PNCH_IN_DT 
        ,substr(POS.CLK_PNCH_IN_DTTM,11,9) AS PNCH_IN_TM
        ,from_timeStamp(days_Add(AddDate(cc_rec_start_date,2),4),'yyyy-MM-dd HH:mm:ss') AS CLK_PNCH_OUT_DTTM --+14400
        ,cast(from_unixtime(unix_timestamp(COALESCE(POS.CLK_PNCH_OUT_DTTM,POS.CLK_PNCH_IN_DTTM))+0) as date) AS PNCH_OUT_DT   --+ INTERVAL '04:00:00' HOUR TO SECOND) AS DATE) AS PNCH_OUT_DT  
        ,substr(from_unixtime(unix_timestamp(COALESCE(POS.CLK_PNCH_OUT_DTTM,POS.CLK_PNCH_IN_DTTM))+0),11,9) AS PNCH_OUT_TM  --+ INTERVAL '04:00:00' HOUR TO SECOND) AS TIME(6)  WITH TIME ZONE) AS PNCH_OUT_TM  
 ,COALESCE(POS.PYRL_TME_TYP_CD,'00103') AS POS_PYRL_CD
        ,POS.CLK_PNCH_EVNT_ID                   AS CLK_PNCH_EVNT_ID
        ,POS.LST_MOD_DTTM                       AS LST_MOD_DTTM
        ,POS.LST_MOD_PTY_ID_VAL       as LST_MOD_PTY_ID_VAL1
        ,POS.LST_MOD_PTY_ID_VAL
        ,POS.REC_INS_DTTM
        ,coalesce(pctt.pyrl_tme_typ_nme,'N/A') as pyrl_tme_typ_nme
        ,coalesce(pctt.prod_pyrl_ind,'N/A') as prod_pyrl_ind
        ,coalesce(pctt.tplh_flg_ind,'N/A') as tplh_flg_ind
 FROM  (select coalesce(case when trim(brnd_cd)='' then null else trim(brnd_cd) end, 'N/A') as brnd_cd
  ,coalesce(case when lgcy_sto_nbr='' then null else lgcy_sto_nbr end, 0) as lgcy_sto_nbr
  ,coalesce(case when clk_pnch_evnt_id='' then null else clk_pnch_evnt_id end, 0) as clk_pnch_evnt_id
  ,coalesce(case when trim(pty_id_val)='' then null else trim(pty_id_val) end, 'N/A') as pty_id_val
  ,coalesce(case when clk_pnch_in_dttm='' then null else clk_pnch_in_dttm end, '9999-12-31 00:00:00') as clk_pnch_in_dttm
  ,coalesce(case when clk_pnch_out_dttm='' then null else clk_pnch_out_dttm end, '9999-12-31 00:00:00') as clk_pnch_out_dttm
  ,coalesce(case when STRLEFT(STrRight(pyrl_tme_typ_cd ,2) ,2)='' then null else trim(pyrl_tme_typ_cd) end, 'N/A') as pyrl_tme_typ_cd
  ,coalesce(case when trim(lst_mod_dttm)='' then null else trim(lst_mod_dttm) end, '9999-12-31 00:00:00') as lst_mod_dttm
  ,coalesce(case when trim(lst_mod_pty_id_val)='' then null else trim(lst_mod_pty_id_val) end, 'N/A') as lst_mod_pty_id_val
  ,coalesce(case when rec_ins_dttm='' then null else rec_ins_dttm end, '9999-12-31 00:00:00') as rec_ins_dttm
from   maprdb_stage0.dy_pyrl_pos_audited_maprdb
where timestamp_cmp(from_timeStamp(days_Add(AddDate(REC_INS_DTTM,2),4),'yyyy-MM-dd HH:mm:ss'),now())
and unix_timestamp(REC_INS_DTTM)>=(select unix_timestamp(job_end_ts)-(5*60) from scct_work_db.aces_audited_job_control))  POS
left outer join (select pos_pyrl_cd, brnd_cd, pyrl_tme_typ_nme, prod_pyrl_ind, tplh_flg_ind from scct_work_db.payroll_company_time_type_tmp3 
group by pos_pyrl_cd, brnd_cd, pyrl_tme_typ_nme, prod_pyrl_ind, tplh_flg_ind) pctt
on POS.pyrl_tme_typ_cd = pctt.POS_PYRL_CD
AND POS.BRND_CD = pctt.BRND_CD)t
)t1;

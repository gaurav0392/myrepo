SELECT COUNT(DISTINCT CS_ORDER_NUMBER) AS order count, SUM(CS_EXT_SHIP_COST) AS total shipping cost, SUM(CS_NET_PROFIT) AS total net profit, DATE_SUB(NOW(), 3)
FROM CATALOG_SALES AS CS1,
DATE_DIM,
CUSTOMER_ADDRESS,
CALL_CENTER
WHERE D_DATE BETWEEN  CAST('1999-4-01' AS DATE) AND DATE_ADD(CAST('1999-4-01' AS DATE), 60) AND CS1.CS_SHIP_DATE_SK = D_DATE_SK AND CS1.CS_SHIP_ADDR_SK = CA_ADDRESS_SK AND CA_STATE = 'ia' AND CS1.CS_CALL_CENTER_SK = CC_CALL_CENTER_SK AND CC_COUNTY IN ('mobile county', 'maverick county', 'huron county', 'kittitas county', 'fairfield county') AND EXISTS (SELECT *
FROM CATALOG_SALES AS CS2
WHERE CS1.CS_ORDER_NUMBER = CS2.CS_ORDER_NUMBER AND CS1.CS_WAREHOUSE_SK <> CS2.CS_WAREHOUSE_SK) AND NOT EXISTS (SELECT *
FROM CATALOG_RETURNS AS CR1
WHERE CS1.CS_ORDER_NUMBER = CR1.CR_ORDER_NUMBER)
ORDER BY COUNT(DISTINCT CS_ORDER_NUMBER)
FETCH NEXT 100 ROWS ONLY

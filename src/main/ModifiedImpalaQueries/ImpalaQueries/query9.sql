WITH SSR AS (SELECT S_STORE_ID AS STORE_ID, SUM(SS_EXT_SALES_PRICE) AS SALES, SUM(COALESCE(SR_RETURN_AMT, 0)) AS returns, SUM(SS_NET_PROFIT - COALESCE(SR_NET_LOSS, 0)) AS PROFIT
FROM STORE_SALES
LEFT JOIN STORE_RETURNS ON SS_ITEM_SK = SR_ITEM_SK AND SS_TICKET_NUMBER = SR_TICKET_NUMBER,
DATE_DIM,
STORE,
ITEM,
PROMOTION
WHERE SS_SOLD_DATE_SK = D_DATE_SK AND D_DATE BETWEEN  CAST('1998-08-04' AS DATE) AND DATE_ADD(CAST('1998-08-04' AS DATE), 30) AND SS_STORE_SK = S_STORE_SK AND SS_ITEM_SK = I_ITEM_SK AND I_CURRENT_PRICE > 50 AND SS_PROMO_SK = P_PROMO_SK AND P_CHANNEL_TV = 'n'
GROUP BY S_STORE_ID), CSR AS (SELECT CP_CATALOG_PAGE_ID AS CATALOG_PAGE_ID, SUM(CS_EXT_SALES_PRICE) AS SALES, SUM(COALESCE(CR_RETURN_AMOUNT, 0)) AS returns, SUM(CS_NET_PROFIT - COALESCE(CR_NET_LOSS, 0)) AS PROFIT
FROM CATALOG_SALES
LEFT JOIN CATALOG_RETURNS ON CS_ITEM_SK = CR_ITEM_SK AND CS_ORDER_NUMBER = CR_ORDER_NUMBER,
DATE_DIM,
CATALOG_PAGE,
ITEM,
PROMOTION
WHERE CS_SOLD_DATE_SK = D_DATE_SK AND D_DATE BETWEEN  CAST('1998-08-04' AS DATE) AND DATE_ADD(CAST('1998-08-04' AS DATE), 30) AND CS_CATALOG_PAGE_SK = CP_CATALOG_PAGE_SK AND CS_ITEM_SK = I_ITEM_SK AND I_CURRENT_PRICE > 50 AND CS_PROMO_SK = P_PROMO_SK AND P_CHANNEL_TV = 'n'
GROUP BY CP_CATALOG_PAGE_ID), WSR AS (SELECT WEB_SITE_ID, SUM(WS_EXT_SALES_PRICE) AS SALES, SUM(COALESCE(WR_RETURN_AMT, 0)) AS returns, SUM(WS_NET_PROFIT - COALESCE(WR_NET_LOSS, 0)) AS PROFIT
FROM WEB_SALES
LEFT JOIN WEB_RETURNS ON WS_ITEM_SK = WR_ITEM_SK AND WS_ORDER_NUMBER = WR_ORDER_NUMBER,
DATE_DIM,
WEB_SITE,
ITEM,
PROMOTION
WHERE WS_SOLD_DATE_SK = D_DATE_SK AND D_DATE BETWEEN  CAST('1998-08-04' AS DATE) AND DATE_ADD(CAST('1998-08-04' AS DATE), 30) AND WS_WEB_SITE_SK = WEB_SITE_SK AND WS_ITEM_SK = I_ITEM_SK AND I_CURRENT_PRICE > 50 AND WS_PROMO_SK = P_PROMO_SK AND P_CHANNEL_TV = 'n'
GROUP BY WEB_SITE_ID) (SELECT CHANNEL, ID, SUM(SALES) AS SALES, SUM(returns) AS returns, SUM(PROFIT) AS PROFIT
FROM (SELECT 'store channel' AS CHANNEL, 'store' || STORE_ID AS ID, SALES, returns, PROFIT
FROM SSR
UNION ALL
SELECT 'catalog channel' AS CHANNEL, 'catalog_page' || CATALOG_PAGE_ID AS ID, SALES, returns, PROFIT
FROM CSR
UNION ALL
SELECT 'web channel' AS CHANNEL, 'web_site' || WEB_SITE_ID AS ID, SALES, returns, PROFIT
FROM WSR) AS X
GROUP BY ROLLUP(CHANNEL, ID))
ORDER BY CHANNEL, ID
FETCH NEXT 100 ROWS ONLY

